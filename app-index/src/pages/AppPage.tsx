import {
    Show,
    Suspense,
    createResource,
    type Component,
    createSignal,
} from "solid-js"

import AppList from "../components/AppList"
import { ErrorPlaceholder } from "../components/ErrorPlaceholder"
import { LoadingSpinner } from "../components/LoadingSpinner"
import { fetchAppList } from "../helpers/api"
import { A, useSearchParams } from "@solidjs/router"
import { AppEntryType } from "../backendSchema"
import AppSeedInstructions from "../components/Instructions/AppSeedInstructions"
import AppSoundInstructions from "../components/Instructions/AppSoundInstructions"
import ManualInstallInstructions from "../components/Instructions/ManualInstallInstructions"
import USBInstallInstructions from "../components/Instructions/USBInstallInstructions"

const AppPage: Component = () => {
    const params = useSearchParams<{ id: string }>()[0]

    const getAppData = async (): Promise<AppEntryType> => {
        const appList = await fetchAppList()
        if (!appList || !params || !params.id) {
            throw "404 App not found!"
        }
        const filteredAppList = appList.apps.filter((app) => {
            return app.repoUrl === params.id
        })[0]
        if (!filteredAppList) {
            throw "404 App not found!"
        }
        return filteredAppList
    }

    const [appData] = createResource(getAppData)
    const [installMode, setInstallMode] = createSignal("seed")

    return (
        <div class="flex min-h-screen flex-col items-center dark:bg-black dark:text-white">
            <div class="w-full pl-5 pt-5">
                <A class="btn" href="/apps/">
                    <i class="fa-solid fa-arrow-left"></i> See all apps
                </A>
            </div>
            <Suspense fallback={<LoadingSpinner />}>
                <Show when={appData()} fallback={<ErrorPlaceholder />}>
                    {(appAccessor) => {
                        const app = appAccessor()
                        return (
                            <div class="flex w-full  max-w-4xl flex-col items-center px-10 py-5">
                                <div>
                                    <h1 class="text-center font-titleBold text-4xl">
                                        {app.name}
                                    </h1>
                                    <span class="mt-5 self-start text-xl font-bold">
                                        About
                                    </span>
                                    <p class="self-start p-5 text-lg">
                                        Author:{" "}
                                        {app.author ? app.author : "<missing>"}
                                        <br />
                                        Description:{" "}
                                        {app.description
                                            ? app.description
                                            : "<no description>"}
                                        <br />
                                        Menu Type: {app.menu} <br />
                                        Version: {app.version} <br />
                                        Commit: {app.commit.substring(0, 12)}...
                                    </p>
                                    <span class="mt-5 self-start text-xl font-bold">
                                        Install
                                    </span>
                                    <div class="flex flex-col items-center">
                                        <div class="relative flex  w-full justify-center gap-2">
                                            <button
                                                class={`tab ${
                                                    installMode() === "seed"
                                                        ? "tab-selected"
                                                        : "tab-unselected"
                                                }`}
                                                onclick={() =>
                                                    setInstallMode("seed")
                                                }
                                            >
                                                Flow3r Seed
                                            </button>
                                            <button
                                                class={`tab ${
                                                    installMode() === "sound"
                                                        ? "tab-selected"
                                                        : "tab-unselected"
                                                }`}
                                                onclick={() =>
                                                    setInstallMode("sound")
                                                }
                                            >
                                                Flow3r Sound
                                            </button>
                                            <Show when={navigator.serial}>
                                                {" "}
                                                <button
                                                    class={`tab ${
                                                        installMode() === "usb"
                                                            ? "tab-selected"
                                                            : "tab-unselected"
                                                    }`}
                                                    onclick={() =>
                                                        setInstallMode("usb")
                                                    }
                                                >
                                                    USB
                                                </button>
                                            </Show>
                                            <button
                                                class={`tab ${
                                                    installMode() === "manual"
                                                        ? "tab-selected"
                                                        : "tab-unselected"
                                                }`}
                                                onclick={() =>
                                                    setInstallMode("manual")
                                                }
                                            >
                                                Manual
                                            </button>
                                        </div>
                                        <div class="mx-5 mt-5 ">
                                            {installMode() === "seed" ? (
                                                <AppSeedInstructions
                                                    appCode={
                                                        app.flow3rSeed
                                                            ? app.flow3rSeed
                                                            : ""
                                                    }
                                                    appName={app.name}
                                                />
                                            ) : installMode() === "sound" ? (
                                                <AppSoundInstructions
                                                    appCode={
                                                        app.flow3rSeed
                                                            ? app.flow3rSeed
                                                            : ""
                                                    }
                                                    appName={app.name}
                                                />
                                            ) : installMode() === "manual" ? (
                                                <ManualInstallInstructions
                                                    downloadUrl={
                                                        app.downloadUrl
                                                    }
                                                />
                                            ) : installMode() === "usb" ? (
                                                <USBInstallInstructions
                                                    app={app}
                                                />
                                            ) : (
                                                ""
                                            )}
                                        </div>
                                    </div>

                                    {/* Footer: Made with :heart: at the badge tent */}
                                    <p class="m-5 text-center">
                                        Made with ❤️ at the badge tent
                                        <br />
                                        <a href="https://git.flow3r.garden/flow3r/web">
                                            Contribute here
                                        </a>
                                    </p>
                                </div>
                            </div>
                        )
                    }}
                </Show>
            </Suspense>
        </div>
    )
}

export default AppPage
